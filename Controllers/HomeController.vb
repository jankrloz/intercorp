﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Return View()
    End Function

    Function Alfombras() As ActionResult
        Return View()
    End Function

    Function Columns() As ActionResult
        Return View()
    End Function

    Function Brintons() As ActionResult
        Return View()
    End Function

    Function Milliken() As ActionResult
        Return View()
    End Function

    Function Forbo() As ActionResult
        Return View()
    End Function

    Function Kahrs() As ActionResult
        Return View()
    End Function

    Function Mohawk() As ActionResult
        Return View()
    End Function

    Function Mts() As ActionResult
        Return View()
    End Function

    Function USV() As ActionResult
        Return View()
    End Function

    Function ValleyForge() As ActionResult
        Return View()
    End Function

    Function SteelBridge() As ActionResult
        Return View()
    End Function

    Function ProTec() As ActionResult
        Return View()
    End Function

    Function About() As ActionResult
        ViewData("Message") = "Your application description page."

        Return View()
    End Function

    Function Contact() As ActionResult
        ViewData("Message") = "Your contact page."

        Return View()
    End Function
End Class
