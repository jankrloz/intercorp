﻿Imports System.Web.Optimization

Public Class MvcApplication
    Inherits System.Web.HttpApplication
    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
        Dim passed As Boolean = True
        If Context.Request.Url.AbsoluteUri.ToLower().EndsWith(".htm") Or Context.Request.Url.AbsoluteUri.ToLower().EndsWith(".swf") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/flash/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/cotizaciones/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/images/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/js/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/less/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("fraganceloader.aspx") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("jpg") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("png") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/css/") Or Context.Request.Url.AbsoluteUri.Contains("/images/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/client/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/seccion/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/services/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/scss/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/fonts/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/upload/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/scripts/") Or Context.Request.Url.AbsoluteUri.ToLower().Contains("/content/") Then
            passed = False
        End If

        If passed Then
            If Context.Request.Url.AbsoluteUri.ToLower().EndsWith("alfombras-para-hoteles.html") Then
                Context.RewritePath("Home/Index", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("alfombras.html") Then
                Context.RewritePath("Home/Alfombras", True)
                passed = False
     
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("mercados.html") Then
                Context.RewritePath("Home/Mercados", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("brintons-axminster-carpet.html") Then
                Context.RewritePath("Home/Brintons", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("milliken-carpet.html") Then
                Context.RewritePath("Home/Milliken", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("forbo-flooring-systems.html") Then
                Context.RewritePath("Home/Forbo", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("kahrs-wood-floors.html") Then
                Context.RewritePath("Home/Kahrs", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("mohawk-carpet.html") Then
                Context.RewritePath("Home/Mohawk", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("mts-seating.html") Then
                Context.RewritePath("Home/Mts", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("us-vinil-wall-covering.html") Then
                Context.RewritePath("Home/USV", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("valley-forge-fabrics.html") Then
                Context.RewritePath("Home/ValleyForge", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("steelbridge-access-floor.html") Then
                Context.RewritePath("Home/SteelBridge", True)
                passed = False
            ElseIf Context.Request.Url.AbsoluteUri.ToLower().EndsWith("pro-tec-surface-protection-systems.html") Then
                Context.RewritePath("Home/ProTec", True)
                passed = False

            End If
        End If

    End Sub
    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub
End Class
