﻿jQuery(document).ready(function () {


    jQuery('.linked').on('click', function () {
        
        
        try {

            var target = jQuery(jQuery(this).attr("data-link"));

            jQuery('html, body').stop().animate({ 'scrollTop': target.offset().top - 18 }, 600, 'easeOutQuart', function () { });

        } catch (exception) { }

        return false;
    });


    var anchorValue;

    var url = document.location;

    var strippedUrl = url.toString().split("#");

    if (strippedUrl.length > 1) {

        anchorvalue = strippedUrl[1];

        if (jQuery("#" + anchorvalue)) {
            jQuery('html, body').stop().animate({
                'scrollTop': jQuery("#" + anchorvalue).offset().top - 18
            }, 600, 'easeOutQuart', function () {
            });
        }
    }
});
