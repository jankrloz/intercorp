﻿
if (jQuery('#royal-slider')) {

    jQuery('#royal-slider').royalSlider({
        arrowsNav: true,
        loop: true,
        keyboardNavEnabled: true,
        controlsInside: true,
        imageScaleMode: 'fit',//fit
        arrowsNavAutoHide: false,
        autoScaleSlider: true,
        autoScaleSliderWidth: 1200,
        autoScaleSliderHeight: 420,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: true,
        startSlideId: 0,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        },
        transitionType: 'move',
        globalCaption: true,
        deeplinking: {
            enabled: true,
            change: false
        },
        usePreloader: true,
        imgWidth: 1200,
        imgHeight: 420
    });

}

if (jQuery('#banner')) {


    jQuery('#banner').royalSlider({
        arrowsNav: true,
        loop: true,
        keyboardNavEnabled: true,
        controlsInside: true,
        imageScaleMode: 'fill',//fit
        arrowsNavAutoHide: false,
        autoScaleSlider: true,
        autoScaleSliderWidth: 1097,
        autoScaleSliderHeight: 353,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: true,
        startSlideId: 0,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        },
        transitionType: 'move',
        globalCaption: true,
        deeplinking: {
            enabled: true,
            change: false
        },
        usePreloader: true,
        imgWidth: 1097,
        imgHeight: 353
    });

}

if (jQuery('#showroom')) {

    jQuery('#showroom').royalSlider({
        arrowsNav: true,
        loop: true,
        keyboardNavEnabled: true,
        controlsInside: true,
        imageScaleMode: 'fill',//fit
        arrowsNavAutoHide: false,
        autoScaleSlider: true,
        autoScaleSliderWidth: 380,
        autoScaleSliderHeight: 322,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: true,
        startSlideId: 0,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        },
        transitionType: 'move',
        globalCaption: true,
        deeplinking: {
            enabled: true,
            change: false
        },
        usePreloader: true,
        imgWidth: 380,
        imgHeight: 322
    });

}



function moveNext(idSlider) {
    jQuery("#" + idSlider).data('royalSlider').next();
}

function movePrev(idSlider) {
    jQuery("#" + idSlider).data('royalSlider').prev();
}


var map_ratio = 1;
function initialize_google_map() {
    if (jQuery("#google-map-0")) {
        var latLng = new google.maps.LatLng(19.431614, -99.201986);

        var mapOptions = {
            center: latLng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE
            },
            mapTypeControl: true
        };

        var map = new google.maps.Map(document.getElementById("google-map-0"), mapOptions);

        var contentString = "Intercorp S.A. de C.V.";

        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: "Intercorp S.A. de C.V."
        });


        map_ratio = jQuery("#google-map-0").width() / 400;
    }
    else {
        alert("nomap")
    }
}

jQuery(window).resize(function () {
    if (jQuery("#google-map-0")) {
        var that = jQuery("#google-map-0");
        var new_height = that.width() / map_ratio;

        if (new_height <= 400) {
            that.css("height", new_height);
        }
    }
});

jQuery(window).ready(initialize_google_map);