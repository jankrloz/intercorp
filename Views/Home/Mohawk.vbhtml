﻿@Code
    ViewData("Title") = "Mohawk"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Mohawk/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Mohawk/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/2mohawk.jpg" />
            <p>
                Mohawk es la compañía fabricante de recubrimientos para pisos más grande del Mundo.
                Con las marcas Karastan, Lees y Bigelow, se presentan soluciones en alfombras comerciales
                para todo tipo de diseños y presupuestos, y con Durkan, se tienen alternativas de diseño,
                color y estilo para generar ambientes hoteleros con creatividad, belleza y buen desempeño.
                Los productos para hospitalidad incluyen, Print, Synthesis, CYP, Merit y modulares.
            </p>
            <p>
                Mohawk is the world`s leading producer and distributor of quality flooring. With the highly
                reputable brands Karastan, Lees and Bigelow we deliver perfect flooring choices by offering
                product solutions for a wide range of markets and budgets. And with Durkan we provide alternatives
                in design, color and style that create hotel environments with creativity, beauty and good performance.
                The hospitality offering includes Print, Synthesis, CYP Merit tufted and modular.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>HOSPITALITY</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Mohawk/Hospitality"))
                            If i < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Mohawk/Hospitality/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CONTRACT</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim j = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Mohawk/Contract"))
                            If j < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Mohawk/Contract/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            j = j + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>