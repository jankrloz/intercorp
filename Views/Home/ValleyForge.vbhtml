﻿@Code
    ViewData("Title") = "Us Vinil"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Valley Forge/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Valley Forge/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/9valleyforge.jpg" />
            <p>
                Valley Forge Fabrics  es una empresa con mas de 30 años como productora de textiles,
                es el principal proveedor en el mundo de telas decorativas de alta calidad para la industria hotelera.
                Durante más de treinta años, la compañía se ha enfocado en la excelencia y la innovación,
                logrando ofrecer así una amplia gama de productos.
            </p>
            <p>
                Valley Forge Fabrics is the world’s largest supplier of high quality decorative upholstery fabrics
                for the hospitality industry. For more than thirty years the company has focused on excellence and
                innovation, providing a diverse range of products.
            </p>
        </div>
    </div>
</div>

<div class="division-middle">&nbsp;</div>
