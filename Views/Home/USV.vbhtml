﻿@Code
    ViewData("Title") = "Us Vinil"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/USV/Banner"))
                                If File.Contains(".jpg") Then
                        @Html.Raw("<div class=""rsContent"">")
                        @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/USV/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                        @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/8usv.jpg" />
            <p>
                US Vinyl es uno de los fabricantes más grandes del mundo de vinil tapiz para recubrimiento
                de paredes para la industria de la hospitalidad.
                El vinil tapiz permite combinar el diseño, color y textura con el desempeño, ya que es resistente,
                fácilmente lavable y cumple con las normas de la industria.
                US Vinyl tiene programas de productos estandarizados para cadenas hoteleras, entre las que se incluyen:
                InterContinental Hotels Group, Marriott, Starwood Hotels and Resorts, Best Western, Choice Hotels International y
                Wyndham Hotels and Resorts.
            </p>
            <p>
                US Vinyl wall coverings is one of the world`s largest manufacturers of vinyl
                wall covering for the hospitality industry. USV has Hotel Standard Programs with
                Intercontinental, Marriott, Best Western, Choice Hotels International, and Wyndham Hotels and Resorts.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CATÁLOGOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/USV/"))
                            If i < 4 Then
                        @Html.Raw("<li><img src=""Images/Alfombras/USV/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>