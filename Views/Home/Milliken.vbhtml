﻿@Code
    ViewData("Title") = "Milliken"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Milliken/Banner"))
                                If File.Contains(".jpg") Then
                        @Html.Raw("<div class=""rsContent"">")
                        @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Milliken/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                        @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/3milliken.jpg" />
            <p>
                Milliken Floor Coverings es parte de Milliken, una de las empresas privadas más grandes del mundo.
                Mediante la combinación de diseño y alta tecnología, Milliken ofrece alfombras en rollo y modulares
                para Hoteles, Oficinas, Casinos, Centros de Convenciones, Gobierno, Cines y Teatros con diseños innovadores,
                calidad, servicio y responsabilidad con el medio ambiente.
            </p>
            <p>
                Milliken Floor Covering is part of Milliken, one of the largest privately held companies in the world.
                Combining high design and cutting edge technology, Milliken offers a broad range of custom designed
                broadloom and carpet tiles for contract and Hospitality, ensuring superior color,
                resistance softness and sustainability.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>HOSPITALITY</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Milliken/Hospitality"))
                            If i < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Milliken/Hospitality/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CONTRACT</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim j = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Milliken/Contract"))
                            If j < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Milliken/Contract/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            j = j + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>