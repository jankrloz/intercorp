﻿@Code
    ViewData("Title") = "Brintons"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Brintons/Banner"))
                                If File.Contains(".jpg") Then
                        @Html.Raw("<div class=""rsContent"">")
                        @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Brintons/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                        @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/1brintons.jpg" />
            <p>
                Brintons, empresa fundada en 1783 en Inglaterra, es el líder a nivel mundial fabricante de
                alfombras Axminster, tanto en rollo como modulares, Wiltons y tapetes de lana hechos a mano.
                Brintons se especializa en ofrecer productos y servicios de calidad, alto desempeño,
                tecnología y diseños innovadores para hoteles en todo el mundo, casinos, entretenimiento,
                cruceros, edificios corporativos, espacios públicos y residencias de lujo.
            </p>
            <p>
                Brintons, a company created in 1783 in England, is the world`s leading manufacturer of woven
                Axminster commercial carpets and tiles, as well as a Wiltons and handcrafted wool rugs.
                Brintons specializes in products that offer quality, performance, service and innovate design,
                and is a market-leading supplier of carpets to the worldwide hospitality, gaming, marine, leisure,
                private, public and residential sectors.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CATÁLOGOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Brintons/Hospitality"))
                            If i < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Brintons/Hospitality/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>