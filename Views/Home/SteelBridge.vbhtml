﻿@Code
    ViewData("Title") = "Us Vinil"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/SteelBridge/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/SteelBridge/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/6steelbridge.jpg" />
            <p>
                El piso elevado SteelBridge está diseñado y fabricado para facilitar el acceso
                instantáneo a los sistemas eléctricos, de voz y de datos debajo del piso, permitiendo
                la productividad al adaptarse a los cambios de una manera rápida, sencilla y de bajo costo.
                Es ideal para oficinas, data centers, casinos y cualquier espacio que requiere la versatilidad
                de tener espacios creativos y eficientes.
            </p>
            <p>
                SteelBridge’s elevated access floors are designed and manufactured to provide instantaneous
                access to the wiring and cabling below the floor, maintaining productivity while changes can
                be made quickly, easily and at a low cost.
                It is ideal for offices, data centers, casinos, and any space that requires the versatility
                of having efficient and creative spaces.
            </p>
        </div>
    </div>

</div>
<div class="division-middle">&nbsp;</div>
