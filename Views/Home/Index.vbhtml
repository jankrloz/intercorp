﻿@Code
    ViewData("Title") = "Home Page"
End Code

@Section othercss
    <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key&#038;sensor=false&#038;ver=3.8.1'></script>

    @Styles.Render("~/Content/royalslider")
    @Styles.Render("~/Content/index")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section


<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/banner/"))
                                If IO.Path.GetExtension(File).ToLower() = ".jpg" Then
                        @Html.Raw("<div class=""rsContent"">")
                        @Html.Raw("<img class=""rsImg"" src=""Images/banner/" + System.IO.Path.GetFileName(File) + """/>")
                        @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container zero-padding" id="cont1">
    <div class="row">
        <div id="productos" class="col-lg-6">
            <span>PRODUCTOS</span>
            <ul>
                <li><a href="alfombras.html">ALFOMBRAS</a></li>
                <li><a href="forbo-flooring-systems.html">PISOS DE LYNOLEUM Y VINÍLICOS</a></li>
                <li><a href="kahrs-wood-floors.html">PISOS, PAREDES Y PLAFONES DE MADERA</a></li>
                <li><a href="mts-seating.html">MOBILIARIO PARA HOTELES</a></li>
                <li><a href="valley-forge-fabrics.html">TEXTILES DECORATIVOS</a></li>
                <li><a href="us-vinil-wall-covering.html">VINIL TAPIZ</a></li>
                <li><a href="steelbridge-access-floor.html">PISO FALSO</a></li>
                <li><a href="pro-tec-surface-protection-systems.html">SISTEMA DE PRTECCIÓN DE SUPERFICIES</a></li>
            </ul>
        </div>
        <div id="marcas" class="col-lg-6">
            <span>MARCAS</span>
            <ul>
                <li><a href="brintons-axminster-carpet.html">@Resources.Intercorp.MarcaBrintons.ToUpper</a></li>
                <li><a href="mohawk-carpet.html">@Resources.Intercorp.MarcaMohawk.ToUpper</a></li>
                <li><a href="milliken-carpet.html">@Resources.Intercorp.MarcaMilliken.ToUpper</a></li>
                <li><a href="forbo-flooring-systems.html">@Resources.Intercorp.MarcaForbo.ToUpper</a></li>
                <li><a href="kahrs-wood-floors.html">@Resources.Intercorp.MarcaKahrs.ToUpper</li>
                <li><a href="mts-seating.html">@Resources.Intercorp.MarcaMts.ToUpper</a></li>
                <li><a href="us-vinil-wall-covering.html">@Resources.Intercorp.MarcaUs.ToUpper</a></li>
                <li><a href="valley-forge-fabrics.html">@Resources.Intercorp.MarcaValleyForge.ToUpper</a></li>
                <li><a href="steelbridge-access-floor.html">@Resources.Intercorp.MarcaSteelbridge.ToUpper</a></li>
                <li><a href="pro-tec-surface-protection-systems.html">@Resources.Intercorp.MarcaProtec.ToUpper</a></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" id="global-quality">
            <span>@Resources.Intercorp.Galardonados</span>
            <img src="~/Images/logo_global_quality.jpg" />
        </div>
    </div>

    <div class="row">
        <div id="imgmarcas" class="col-lg-12">
            <ul>
                @Code
                    For Each File In (From p In System.IO.Directory.GetFiles(Server.MapPath("~/Images/marcas/")) Order By p Ascending)
                @Html.Raw("<li><img src=""Images/marcas/" + System.IO.Path.GetFileName(File) + """/></li>")
                    Next
                End Code
            </ul>
        </div>
    </div>

</div>

<div class="horizontal-division"></div>

<div id="cont2" class="container zero-padding">
    <div id="nosotros" class="row">
        <div id="imgdescr" class="col-lg-6">
            <img src="Images/mapa.png" />
        </div>
        <div id="txtdescr" class="col-lg-6">
            <div class="row">
                <div id="us-logo" class="col-lg-12">
                    <img src="Images/Logo_intercorp.jpg" alt="logo" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 zero-padding">
                    <p>@Resources.Intercorp.NosotrosP1</p>
                    <p>@Resources.Intercorp.NosotrosP2</p>
                    <p>@Resources.Intercorp.NosotrosP3</p>
                    <p>@Resources.Intercorp.NosotrosP4</p>
                    <p>@Resources.Intercorp.NosotrosP5</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<p id="nuestrosclientes">@Resources.Intercorp.NuestrosClientes</p>
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div id="royal-slider" class="slider royalSlider rsMinW rsWhite alpha ">
                @Code
                    Dim i As Integer = 0
                    Dim count As Integer = 0
                    Dim totalCount As Integer = (From p In System.IO.Directory.GetFiles(Server.MapPath("~/Images/clientes/")) Where p.Contains(".jpg") Select p).Count()
                    For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/clientes/"))

                        If IO.Path.GetExtension(File).ToLower() = ".jpg" Then
                            If i = 0 Then
                @Html.Raw("<div class=""rsContent""><ul>")
                            End If

                            If i < 30 Then
                @Html.Raw("<li><img src=""Images/clientes/" + System.IO.Path.GetFileName(File) + """/></li>")
                                i += 1

                                If (count + 1 = totalCount) Then
                @Html.Raw("</ul></div>")
                                End If

                            Else
                @Html.Raw("</ul></div>")
                                i = 0
                            End If
                            count = count + 1
                        End If
                    Next
                End Code
            </div>
            <div class="rs-arrows-clear">
                <div class="left" onclick="movePrev('royal-slider');">&nbsp;</div>
                <div class="right" onclick="moveNext('royal-slider');">&nbsp;</div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div id="light-gray-bg">
    <img id="eco" src="Images/ecofriendly.jpg" alt="Ecofriendly" />
</div>

<div class="horizontal-division"></div>

<div class="container" id="formulario">
    <div class="row">
        <div class="contacto-info col-xs-12">
            @Resources.Intercorp.DeseaRecibirInfo
        </div>
    </div>
    <div class="row">
        <div class="contacto-info col-xs-12">
            <u>@Resources.Intercorp.InformacionContacto</u>
        </div>
    </div>
    <div class="row">
        <form method="post">
            <div class="col-lg-4">
                <label for="nombre">@Resources.Intercorp.Nombre</label><input type="text" name="nombre" id="nombre" value="">
                <label for="compania">@Resources.Intercorp.Compania</label><input type="text" name="compania" id="compania" value="">
                <label for="ciudad">@Resources.Intercorp.Ciudad</label><input type="text" name="ciudad" id="ciudad" value="">
                <label for="estado">@Resources.Intercorp.Estado</label><input type="text" name="estado" id="estado" value="">
                <label for="tel">@Resources.Intercorp.Telefono</label><input type="tel" name="tel" id="tel" value="">
                <label for="correo">@Resources.Intercorp.CorreoElectronico</label> <input type="email" name="correo" id="correo" value="">
            </div>
            <div class="col-lg-4">
                <label for="apellido">@Resources.Intercorp.Apellidos</label><input type="text" name="apellido" id="apellido" value="">
                <label for="pais">@Resources.Intercorp.Pais</label><input type="text" name="pais" id="pais" value="">
            </div>
            <div class="col-lg-4">
                <div id="label-mensaje">
                    <label for="mensaje">@Resources.Intercorp.SuMensaje</label>
                </div>
                <textarea name="mensaje" id="mensaje"></textarea>
                <br /><br />
                <input type="submit" name="enviar" id="enviar" value="@Resources.Intercorp.Enviar">
            </div>
        </form>
    </div>
</div>

<div class="bg-graylight">
    <div class="container zero-padding" id="map">
        <div class="row">
            <div class="col-lg-8">
                <div id="google-map-0"></div>
            </div>
            <div class="col-lg-4" id="mapdir">
                <span>
                    @Html.Raw(Resources.Intercorp.TextoMapa)
                </span>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12 no-padding">
            <div id="showroom-container">
                <span>NUESTRO SHOWROOM</span>
            </div>
            <div id="showroom" class="slider royalSlider rsMinW rsWhite alpha ">
                <div class="rsContent">
                    <img class="rsimg" src="images/Showroom/1.jpg" />
                    <img class="rsimg" src="images/Showroom/2.jpg" />
                    <img class="rsimg" src="images/Showroom/3.jpg" />
                </div>
                <div class="rsContent">
                    <img class="rsimg" src="images/Showroom/3.jpg" />
                    <img class="rsimg" src="images/Showroom/4.jpg" />
                    <img class="rsimg" src="images/Showroom/5.jpg" />
                </div>
            </div>
            <div class="rs-arrows-clear">
                <div class="left" onclick="movePrev('showroom');">&nbsp;</div>
                <div class="right" onclick="movePrev('showroom');">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
