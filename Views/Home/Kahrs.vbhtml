﻿@Code
    ViewData("Title") = "Kahrs"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Kahrs/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Kahrs/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container no-padding">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/5kahrs.jpg" />
            <p>
                Kährs es el fabricante de pisos de madera más grande de Europa y una de las empresas más innovadoras
                del mundo dedicada al tratamiento de la madera, y mediante la combinación de una larga tradición que
                data de 1857, un apasionado interés en el diseño y las nuevas tecnologías, se ha mantenido a la
                vanguardia en el desarrollo de pisos de madera modernos.
                Es el inventor del primer piso de ingeniería, es la primera empresa en ofrecer pisos con pre-acabado
                de fabrica y es la primera empresa en obtener certificados de calidad y responsabilidad con el medio ambiente.
            </p>
            <p>
                Kahrs group is Europe`s largest wood flooring manufacturer, and one of the world`s most innovative
                companies involved in the processing of wood. The combination of a long tradition, craftsmanship and
                a passionate interest in design and new technology has kept Kahrs at the forefront in the development
                of modern wood flooring.
                Kahrs invented the world`s first engineered wood floor, was the first company to offer a factory-applied
                pre-finish, and was the first company to earn certifications in quality and environmental responsibility.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CATÁLOGOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Kahrs"))
                            If i < 4 Then
                        @Html.Raw("<li><img src=""Images/Alfombras/Kahrs/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>