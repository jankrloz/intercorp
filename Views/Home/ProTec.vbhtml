﻿@Code
    ViewData("Title") = "Us Vinil"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/ProTec/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/ProTec/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/Alfombras/ProTec/logo_secc.jpg" />
            <p>
                Es una empresa de materiales para la protección temporal de superficies
                durante la construcción o remodelación de proyectos. Es mas fácil, rápido y
                económico proteger las superficies que limpiarlas, lavarlas, repararlas o reponerlas
                cuando son dañadas.
            </p>
            <p>
                It`s a company specialized in materials for temporary surface protection during construction
                or remodeling projects. It is easier, faster and cheaper to protect surfaces instead of cleaning,
                washing, repairing or replacing them when damaged.
            </p>
        </div>
    </div>

    
</div>

<div class="division-middle">&nbsp;</div>
