﻿@Code
    ViewData("Title") = "Forbo"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Forbo/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/Forbo/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/4forbo.jpg" />
            <p>
                Forbo Flooring Systems es el líder mundial en revestimientos para pisos de linóleum con
                60% del mercado mundial. Adicionalmente desarrolla pisos vinílicos de alta calidad, Flotex,
                que es un recubrimiento textil de diseño para pisos, y sistemas de entrada.
                Todos los productos son fabricados especialmente para hospitales, educación, oficinas, comercios,
                gobierno, residencial y transporte.
            </p>
            <p>
                With over 60% market share, Forbo Flooring Systems is the global market leader in Linoleum floor coverings.
                Additionally it develops high quality vinyl, textile floor coverings and entrance systems for healthcare,
                education, corporate, retail, residential and transportation.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CATÁLOGOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Forbo/"))
                            If i < 4 Then
                    @Html.Raw("<li><img src=""Images/Alfombras/Forbo/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>