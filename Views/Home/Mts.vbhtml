﻿@Code
    ViewData("Title") = "Mts"
End Code

@Section othercss
    @Styles.Render("~/Content/vistas-marcas")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section

@Section scripts
    @Scripts.Render("~/bundles/royalslider")
End Section

<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/mts/Banner"))
                                If File.Contains(".jpg") Then
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/Alfombras/mts/Banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                                End If
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="descr-marca col-lg-12">
            <img class="logomarca" src="~/Images/marcas/7mts.jpg" />
            <p>
                Con mas de 50 años de historia, MTS es líder a nivel mundial de mobiliario para la industria
                de la hospitalidad. Ofreciendo la mas alta calidad y diseños innovadores en todo tipo de sillas y mesas.
                Sus productos se pueden encontrar en los mejores restaurantes, centros de convenciones y salones de eventos.
                Los productos de MTS ofrecen rendimiento, diseño, confort y valor incomprable.
            </p>
            <p>
                With over 50 years experience, MTS is the worldwide leader in the hospitality furniture industry.
                Offering the highest quality and innovative design in all types of chairs and tables, their products
                can be found from the best restaurants to the largest convention centers and ballrooms.
                MTS products offer performance, design, comfort and unparalleled value.
            </p>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12 titulos-thumbs">
            <p>CATÁLOGOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-9 col-lg-offset-2">
            <div class="thumbs">
                <ul>
                    @Code
                        Dim i = 0
                        For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/Mts/"))
                            If i < 4 Then
                        @Html.Raw("<li><img src=""Images/Alfombras/Mts/" + System.IO.Path.GetFileName(File) + """/></li>")
                            End If

                            i = i + 1
                        Next
                    End Code
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 ver-mas">
            <a href="#">ver mas...</a>
        </div>
    </div>
</div>