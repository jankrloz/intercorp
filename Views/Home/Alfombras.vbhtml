﻿@Code
    ViewData("Title") = "Mohawk"
End Code

@Section othercss
    @Styles.Render("~/Content/alfombras")
    @Styles.Render("~/Content/royalslider")

    <link href="~/Scripts/royal/skins/minimal-white/rs-minimal-white-2.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
            background: none !important;
        }

        #banner .rsImg {
            margin-top: 0 !important;
        }

        .no-padding {
            padding: 0;
        }

        .rsBullets {
            display: none !important;
        }
    </style>

End Section


<div class="bg-gray">
    <div class="container margin-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-container">
                    <div id="banner" class="slider royalSlider rsMinW rsWhite alpha ">
                        @Code
                            For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/banner/"))
                            @Html.Raw("<div class=""rsContent"">")
                            @Html.Raw("<img class=""rsImg"" src=""Images/banner/" + System.IO.Path.GetFileName(File) + """/>")
                            @Html.Raw("</div>")
                            Next
                        End Code
                    </div>
                </div>
                <div class="rs-arrows">
                    <div class="left" onclick="movePrev('banner');">&nbsp;</div>
                    <div class="right" onclick="moveNext('banner');">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="horizontal-division"></div>

<div class="container">
    <div class="row">
        <div class="titles col-lg-5 col-lg-offset-2">
            <span>HOSPITALITY</span>
        </div>
        <div class="titles col-lg-5">
            <span>CONTRACT</span>
        </div>
    </div>
    
    @code
        
        Dim marcas_alfombras() As String = {"Brintons", "Mohawk", "Milliken"}
        
        For Each marca_alfombra In marcas_alfombras
            
            @Html.Raw("<div class=""row"">") 'Fila de Marca y portadas
            
            @Html.Raw("<div class=""logo-marca col-lg-2"">")
            Select Case marca_alfombra
                
                Case "Brintons"
                    @Html.Raw("<img src=""Images/marcas/1" + marca_alfombra.ToLower + ".jpg"" />")        
                Case "Mohawk"
                    @Html.Raw("<img src=""Images/marcas/2" + marca_alfombra.ToLower + ".jpg"" />")
                Case "Milliken"
                    @Html.Raw("<img src=""Images/marcas/3" + marca_alfombra.ToLower + ".jpg"" />")
            End Select
            
            @Html.Raw("</div>")
                
            
            If System.IO.Directory.Exists(Server.MapPath("~/Images/Alfombras/" + marca_alfombra + "/Hospitality")) Then
                @Html.Raw("<div class=""portada col-lg-5"" id=""port-h-" + marca_alfombra.ToLower + """>")
                @Html.Raw("<img src=""Images/Alfombras/" + marca_alfombra + "/Hospitality/portada.jpg"" />")
                @Html.Raw("</div>")
            End If
            

            If System.IO.Directory.Exists(Server.MapPath("~/Images/Alfombras/" + marca_alfombra + "/Contract")) Then
                @Html.Raw("<div class=""portada col-lg-5"" id=""port-c-" + marca_alfombra.ToLower + """>")
                @Html.Raw("<img src=""Images/Alfombras/" + marca_alfombra + "/Contract/portada.jpg"" />")
                @Html.Raw("</div>")
            End If
            
            @Html.Raw("</div>") 'Fin de Fila de Marca y portadas
            
            @Html.Raw("<div class=""row"">") 'Fila de Catalogos

            If System.IO.Directory.Exists(Server.MapPath("~/Images/Alfombras/" + marca_alfombra + "/Hospitality")) Then
                
                'Catalogos de Hospitality

                @Html.Raw("<div class=""catalogos col-lg-5 col-lg-offset-2"" id=""cat-h-" + marca_alfombra.ToLower + """>")

                Dim i = 0

                For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/" + marca_alfombra + "/Hospitality"))
                    If Not System.IO.Path.GetFileName(File).Equals("portada.jpg") Then
                        @Html.Raw("<img class=""thumb"" src=""Images/Alfombras/" + marca_alfombra + "/Hospitality/" + System.IO.Path.GetFileName(File) + """/>")
                    End If
                Next
                
                @Html.Raw("</div>")
                
            End If
            
            If System.IO.Directory.Exists(Server.MapPath("~/Images/Alfombras/" + marca_alfombra.ToLower + "/Contract")) Then
            
                'Catalogos de Contract
                
                @Html.Raw("<div class=""catalogos col-lg-5"" id=""cat-c-" + marca_alfombra.ToLower + """>")

                Dim i = 0

                For Each File In System.IO.Directory.GetFiles(Server.MapPath("~/Images/Alfombras/" + marca_alfombra + "/Contract"))
                    If Not System.IO.Path.GetFileName(File).Equals("portada.jpg") Then
                        @Html.Raw("<img class=""thumb"" src=""Images/Alfombras/" + marca_alfombra + "/Contract/" + System.IO.Path.GetFileName(File) + """/>")
                    End If
                Next
                
                @Html.Raw("</div>")
                
            End If
            
            @Html.Raw("</div>") 'Fin de Fila de Catalogos

        Next

    End Code

</div>

@Section scripts
    @Scripts.Render("~/bundles/alfombras")
    @Scripts.Render("~/bundles/royalslider")
End Section