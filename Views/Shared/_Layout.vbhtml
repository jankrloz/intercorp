﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - Intercorp</title>
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")

    @RenderSection("othercss", required:=False)

    <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC:900,400,300italic,500italic,900italic|Alegreya+Sans:400,300italic,700italic|Raleway:400,300' rel='stylesheet' type='text/css'>

    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->
    <style type="text/css">
        .rsArrow{
            display:none!important;
        }
    </style>
    <meta name="description" content="The description of my page" />
</head>
<body>

    <header class="container">
        <div class="row">
            <div id="logo" class="col-lg-6 col-xs-12"><img src="Images/Logo_intercorp.jpg" /></div>
            <ul id="menu1" class="col-lg-6 col-xs-12">
                <li><a href="#">carpet</a> |</li>
                <li><a href="#">wood</a> |</li>
                <li><a href="#">flooring</a> |</li>
                <li><a href="#">fabric</a> |</li>
                <li class="last"><a href="#">furniture</a> </li>
            </ul>
        </div>
        <div class="row">
            <ul id="menu2" class="col-lg-12">
                <li><a href="~/alfombras-para-hoteles.html">@Resources.Intercorp.MenuInicio</a></li>
                <li data-link="#nosotros" class="linked">@Resources.Intercorp.MenuNosotros</li>
                <li data-link="#productos" class="linked">@Resources.Intercorp.MenuMercados</li>
                <li data-link="#nuestrosclientes" class="linked">@Resources.Intercorp.MenuClientes</li>
                <li data-link="#eco" class="linked">@Resources.Intercorp.MenuAmbiente</li>
                <li data-link="#formulario" class="linked">@Resources.Intercorp.MenuContacto</li>
                <li id="companyinfo">@Resources.Intercorp.MenuEnglish</li>
            </ul>
        </div>
    </header>


    <div class="horizontal-division"></div>
    @RenderBody()

    <div class="horizontal-division"></div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <span class="footer-titles">@Resources.Intercorp.Empresa</span>
                    <ul>
                        <li><a href="~/alfombras-para-hoteles.html">@Resources.Intercorp.MenuInicio</a></li>
                        <li>@Resources.Intercorp.MenuNosotros</li>
                        <li>@Resources.Intercorp.MenuMercados</li>
                        <li>@Resources.Intercorp.MenuClientes</li>
                        <li>@Resources.Intercorp.MenuAmbiente</li>
                        <li>@Resources.Intercorp.MenuContacto</li>
                    </ul>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <span class="footer-titles">@Resources.Intercorp.Productos</span>
                    <ul>
                        <li><a href="alfombras.html">@Resources.Intercorp.Alfombras</li>
                        <li><a href="forbo-flooring-systems.html">@Resources.Intercorp.PisosLynoleumVinilicos</a></li>
                        <li><a href="kahrs-wood-floors.html">@Resources.Intercorp.PisosParedesPlafonesMadera</a></li>
                        <li><a href="mts-seating.html">@Resources.Intercorp.MobiliarioHoteles</a></li>
                        <li><a href="valley-forge-fabrics.html">@Resources.Intercorp.TextilesDecorativos</a></li>
                        <li><a href="us-vinil-wall-covering.html">@Resources.Intercorp.VinilTapiz</a></li>
                        <li><a href="steelbridge-access-floor.html">@Resources.Intercorp.PisoFalso</a></li>
                        <li><a href="pro-tec-surface-protection-systems.html">@Resources.Intercorp.SistemaProteccionSuperficies</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <span class="footer-titles">@Resources.Intercorp.Marcas</span>
                    <ul>
                        <li><a href="brintons-axminster-carpet.html">@Resources.Intercorp.MarcaBrintons</a></li>
                        <li><a href="mohawk-carpet.html">@Resources.Intercorp.MarcaMohawk</a></li>
                        <li><a href="milliken-carpet.html">@Resources.Intercorp.MarcaMilliken</a></li>
                        <li><a href="forbo-flooring-systems.html">@Resources.Intercorp.MarcaForbo</a></li>
                        <li><a href="kahrs-wood-floors.html">@Resources.Intercorp.MarcaKahrs</a></li>
                        <li><a href="mts-seating.html">@Resources.Intercorp.MarcaMts</a></li>
                        <li><a href="us-vinil-wall-covering.html">@Resources.Intercorp.MarcaUs</a></li>
                        <li><a href="valley-forge-fabrics.html">@Resources.Intercorp.MarcaValleyForge</a></li>
                        <li><a href="steelbridge-access-floor.html">@Resources.Intercorp.MarcaSteelbridge</a></li>
                        <li><a href="pro-tec-surface-protection-systems.html">@Resources.Intercorp.MarcaProtec</a></li>
                    </ul>
                </div>
                <div class="last-footer col-lg-3 col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="footer-titles">@Resources.Intercorp.ShowRoomOficinaCorporativa</span>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <span>@Html.Raw(Resources.Intercorp.DireccionMatriz)</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="Images/logo_american.jpg" />
                        </div>
                        <div class="col-lg-6" id="fb">
                            <img src="Images/fb.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="horizontal-division last"></div>

        <div class="container">
            <div class="row" id="footerbottom">
                <div class="col-lg-6">
                    <span>@Html.Raw(Resources.Intercorp.PoliticaPrivacidad)</span>
                </div>

                <div class="col-lg-6" id="empresa">
                    <span>@Html.Raw(Resources.Intercorp.Legales)</span>
                </div>
            </div>
        </div>

    </footer>

    
   

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/form")
    @RenderSection("scripts", required:=False)
    


    @Code


        Dim isIndex As Boolean = False
        If HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("home/index") >= 0 Or HttpContext.Current.Request.Url.AbsoluteUri.ToLower().IndexOf("alfombras-para-hoteles.html") >= 0 Then
            isIndex = True
        End If
    
        
        
    
        If (isIndex) Then
            @Html.Raw("<script type=""text/javascript"" src=""Scripts/make_transition.js""></script>")
        Else
            @Html.Raw("<script type=""text/javascript"" src=""Scripts/navigate_transition.js""></script>")
        End If
        
        
End Code

    @Scripts.Render("~/bundles/adwebsys")
</body>
</html>
