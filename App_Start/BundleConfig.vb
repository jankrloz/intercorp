﻿Imports System.Web.Optimization

Public Module BundleConfig
    ' Para obtener más información sobre las uniones, visite http://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-2.1.1.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*"))

        ' Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
        ' preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"))

        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
                  "~/Scripts/bootstrap.js",
                  "~/Scripts/respond.js"))

        bundles.Add(New StyleBundle("~/Content/css").Include(
                  "~/Content/bootstrap.css",
                  "~/Content/font-awesome.min.css",
                  "~/Content/site.css"))

        bundles.Add(New ScriptBundle("~/bundles/form").Include("~/Scripts/form/easing.js").Include("~/Scripts/form/effects.js").Include("~/Scripts/form/prototype.js").Include("~/Scripts/form/form.js").Include("~/Scripts/form/captcha.js").Include("~/Scripts/form/translate.js").Include("~/Scripts/form/validation.js"))



        bundles.Add(New ScriptBundle("~/bundles/royalslider").Include("~/Scripts/royal/jquery.royalslider.min.js"))
        bundles.Add(New ScriptBundle("~/bundles/adwebsys").Include("~/Scripts/adwebsys.js"))
        bundles.Add(New ScriptBundle("~/bundles/alfombras").Include("~/Scripts/alfombras.js"))

        bundles.Add(New StyleBundle("~/Content/index").Include("~/Content/index.css"))
        bundles.Add(New StyleBundle("~/Content/vistas-marcas").Include("~/Content/vistas-marcas.css"))
        bundles.Add(New StyleBundle("~/Content/alfombras").Include("~/Content/alfombras.css"))
        bundles.Add(New StyleBundle("~/Content/royalslider").Include("~/Scripts/royal/royalslider.css"))

        ' Para la depuración, establezca EnableOptimizations en false. Para obtener más información,
        ' visite http://go.microsoft.com/fwlink/?LinkId=301862
        BundleTable.EnableOptimizations = True
    End Sub
End Module

